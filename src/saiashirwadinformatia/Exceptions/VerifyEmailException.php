<?php

namespace SaiAshirwadInformatia\Exceptions;

/**
 * verifyEmail exception handler
 */
class VerifyEmailException extends \Exception
{

    /**
     * Prettify error message output
     * @return string
     */
    public function errorMessage()
    {
        $errorMsg = $this->getMessage();
        return $errorMsg;
    }

}
