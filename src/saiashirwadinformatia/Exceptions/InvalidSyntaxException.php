<?php

namespace SaiAshirwadInformatia\Exceptions;

class InvalidSyntaxException extends \Exception
{

    /**
     * @param $email
     */
    public static function create($email)
    {
        throw new self("The syntax of email address `{$email}` is not valid");
    }
}
