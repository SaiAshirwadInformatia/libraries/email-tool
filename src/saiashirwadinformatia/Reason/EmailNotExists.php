<?php

namespace SaiAshirwadInformatia\Reason;

use Egulias\EmailValidator\Result\Reason\Reason;

class EmailNotExists implements Reason
{
    private $reason;

    public function __construct($reason)
    {
        $this->reason = $reason;
    }
    /**
     * @return int
     */
    public function code(): int
    {
        return 0;
    }

    public function description(): string
    {
        return $this->reason;
    }
}
