<?php

namespace SaiAshirwadInformatia\Validations;

use Egulias\EmailValidator\EmailLexer;
use Egulias\EmailValidator\EmailParser;
use Egulias\EmailValidator\Result\InvalidEmail;
use Egulias\EmailValidator\Result\Reason\ExceptionFound;
use Egulias\EmailValidator\Validation\EmailValidation;
use SaiAshirwadInformatia\Reason\EmailNotExists;
use SaiAshirwadInformatia\Services\VerifyEmail;
use SaiAshirwadInformatia\Warnings\SkipDomainCheck;

class EmailExistsValidation implements EmailValidation
{
    /**
     * @var EmailParser|null
     */
    private $parser;

    /**
     * @var array
     */
    private $warnings = [];

    /**
     * @var ?InvalidEmail
     */
    private $error;

    /**
     * @var mixed
     */
    private $debug;

    /**
     * @var mixed
     */
    private $exceptions;

    /**
     * @param $debug
     */
    public function __construct($debug = false, $exceptions = false)
    {
        $this->debug      = (boolean) $debug;
        $this->exceptions = (boolean) $exceptions;
    }

    /**
     * @param string     $email
     * @param EmailLexer $emailLexer
     */
    public function isValid(string $email, EmailLexer $emailLexer): bool
    {
        $this->parser = new EmailParser($emailLexer);
        try {
            $result = $this->parser->parse($email);

            $this->warnings = $this->parser->getWarnings();
            if ($result->isInvalid()) {
                /*
                 * @psalm-suppress PropertyTypeCoercion
                 */
                $this->error = $result;
                return false;
            }

            $verify = new VerifyEmail($this->exceptions, $this->debug);

            $valid = $verify->check($email);
            if (!$valid) {
                if (strpos($verify->get_error(), "Skipping") !== false) {
                    $this->warnings = [new SkipDomainCheck($verify->get_error())];
                    $valid = true;
                } else {
                    $reason      = new EmailNotExists($verify->get_error());
                    $this->error = new InvalidEmail($reason, "");
                }
            }
            return $valid;
        } catch (\Exception $invalid) {
            $this->error = new InvalidEmail(new ExceptionFound($invalid), '');
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getError(): ?InvalidEmail
    {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getWarnings(): array
    {
        return $this->warnings;
    }

}
