<?php

namespace SaiAshirwadInformatia\Models;

use SaiAshirwadInformatia\Exceptions\InvalidSyntaxException;

class Email
{

    /**
     * @var mixed
     */
    protected $username;

    /**
     * @var mixed
     */
    protected $domain;

    /**
     * @var mixed
     */
    protected $email;

    /**
     * @var mixed
     */
    protected $isValid = null;

    /**
     * @var mixed
     */
    protected $exists = null;

    /**
     * @param $email
     */
    public function __construct($email)
    {
        if (strpos($email, '@') === false) {
            InvalidSyntaxException::create($email);
        }

    }
}
