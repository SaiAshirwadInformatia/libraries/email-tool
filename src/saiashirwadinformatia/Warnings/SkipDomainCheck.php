<?php

namespace SaiAshirwadInformatia\Warnings;

use Egulias\EmailValidator\Warning\Warning;

class SkipDomainCheck extends Warning
{
    /**
     * @param $domain
     */
    public function __construct($message)
    {
        $this->message = $message;
    }
}
