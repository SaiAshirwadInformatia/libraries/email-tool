<?php
namespace SaiAshirwadInformatia;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use SaiAshirwadInformatia\Validations\EmailExistsValidation;

class EmailTool
{
    /**
     * @var mixed
     */
    protected $checkDNS = true;

    /**
     * @var mixed
     */
    protected $checkExists = true;

    /**
     * @var mixed
     */
    protected $debug = false;

    /**
     * @var mixed
     */
    protected $exceptions = false;

    /**
     * @var mixed
     */
    protected $validator = null;

    public function __construct()
    {
        $this->validator = new EmailValidator;
    }

    /**
     * @return mixed
     */
    public function throwExceptions()
    {
        $this->exceptions = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function suppressExceptions()
    {
        $this->exceptions = false;

        return $this;
    }

    /**
     * @return mixed
     */
    public function enableDebug()
    {
        $this->debug = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function disableDebug()
    {
        $this->debug = false;

        return $this;
    }

    /**
     * @return mixed
     */
    public function enableDNSCheck()
    {
        $this->checkDNS = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function disableDNSCheck()
    {
        $this->checkDNS = false;

        return $this;
    }

    /**
     * @return mixed
     */
    public function enableExistsCheck()
    {
        $this->checkExists = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function disableExistsCheck()
    {
        $this->checkExists = false;

        return $this;
    }

    /**
     * @param $email
     */
    public function isValid($email)
    {
        $validators = [
            new RFCValidation,
        ];

        if ($this->checkDNS) {
            $validators[] = new DNSCheckValidation;
        }
        if ($this->checkExists) {
            $validators[] = new EmailExistsValidation($this->debug, $this->exceptions);
        }
        $multipleValidations = new MultipleValidationWithAnd($validators);
        $valid               = $this->validator->isValid($email, $multipleValidations);

        return $valid;
    }

    /**
     * @return boolean
     */
    public function hasWarnings()
    {
        return !empty($this->getWarnings());
    }

    /**
     * @return array
     */
    public function getWarnings()
    {
        return $this->validator->getWarnings();
    }

    /**
     * @return InvalidEmail|null
     */
    public function getError()
    {
        return $this->validator->getError();
    }

}
