# Email Tool

A complete email tool to verify email address and their existence.

## References

1. For standard validations used [egulias/email-validator](https://github.com/egulias/EmailValidator), thanks Egulias for amazing package
2. For existence of email verification referenced from [CodexWorld](https://www.codexworld.com/verify-email-address-check-if-real-exists-domain-php/)

## Installation

```
composer require saiashirwadinformatia/email-tool
```

# License

MIT License

# Copyright

Sai Ashirwad Informatia, 2021
