---
home: true
heroImage: /images/sai_logo.png
tagline: Validate Email Address and it&#39;s Existence
actionText: Quick Start →
actionLink: /guide/
features:
- title: RFC Validation
  details: Check with all standard RFC rules for syntax validation
- title: DNS Validation
  details: Check with existence of host & verify MX records
- title: Existence
  details: Check whether the email address really exists
footer: Made by Rohan Sakhale with ❤️
---
