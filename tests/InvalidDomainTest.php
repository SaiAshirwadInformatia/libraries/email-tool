<?php

use SaiAshirwadInformatia\EmailTool;

$invalidDomains = [
    "foxmail.com" => "Unable to get DNS records for the host",
    "gmal.com"    => "No MX or A DSN record was found for this email",
];

foreach ($invalidDomains as $domain => $error) {
    test("check invalid domain '$domain' gives error as $error", function () use ($domain, $error) {
        $tool  = new EmailTool;
        $email = "rohansakhale@$domain";
        $valid = $tool->isValid($email);

        $expectedError = $error . "\n";

        // Check this is invalid
        expect($valid)->toBeFalse();

        // Check the error message is correct as expected

        $this->assertEquals($tool->getError()->description(), $expectedError);
    });
}
