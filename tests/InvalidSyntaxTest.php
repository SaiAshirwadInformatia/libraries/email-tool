<?php

use SaiAshirwadInformatia\EmailTool;

$invalidUsernames = [
    "rohans..sakhale" => "Concecutive DOT found",
    ".rohansakhale"   => "Starts with a DOT",
    "rohansakhale["   => "Expecting ATEXT (Printable US-ASCII). Extended: Invalid token found",
    "rohan]sakhale"   => "Expecting ATEXT (Printable US-ASCII). Extended: Invalid token found",
];

foreach ($invalidUsernames as $username => $error) {
    test("check invalid username '$username' gives error as $error", function () use ($username, $error) {
        $tool  = new EmailTool;
        $email = "$username@gmail.com";

        $valid = $tool->isValid($email);

        $expectedError = $error . "\n";

        // Check this is invalid
        expect($valid)->toBeFalse();

        // Check the error message is correct as expected
        $this->assertEquals($tool->getError()->description(), $expectedError);
    });
}
