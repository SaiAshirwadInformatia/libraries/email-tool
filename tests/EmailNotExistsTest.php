<?php

use SaiAshirwadInformatia\EmailTool;

$wrongEmails = [
    "rohansakhalesakhalerohan@gmail.com" => "550-5.1.1 The email account that you tried to reach does not exist. Please try",
    "rohansakhalesakhalerohan@ymail.com" => "550-5.1.1 The email account that you tried to reach does not exist. Please try",
];
foreach ($wrongEmails as $email => $error) {
    test("check email not exists for $email", function () use ($email, $error) {

        $tool = new EmailTool;

        $valid = $tool->enableDebug()->isValid($email);

        $expectedError = $error . "\n";

        // Check this is invalid
        expect($valid)->toBeFalse();

        // Check the error message is correct as expected
        $this->assertEquals($tool->getError()->description(), $expectedError);
    });

}
